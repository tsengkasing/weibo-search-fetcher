const mysql = require('mysql2/promise');

class DB {
    constructor(host, user, password, database) {
        this._config = {
            host,
            user,
            password,
            database
        };

        this._connection = null;
    }

    async connect() {
        this._connection = await mysql.createConnection(this._config);
    }

    /**
     * Fetch one row
     * @param {string} query SQL
     * @param {Array<string | number>} param parameters
     */
    async fetchOne(query, param) {
        const [rows] = await this._connection.query(query, param);
        return rows[0];
    }

    async fetchAll(query, param) {
        const [rows] = await this._connection.query(query, param);
        return rows;
    }

    async execute(query, param) {
        try {
            await this._connection.execute(query, param);
            return {ok: true};
        } catch (err) {
            return {ok: false, msg: err};
        }
    }
}

module.exports = DB;

/* (async function () {
    const db = new DB('localhost', 'root', 'mysql', 'weibo');
    await db.connect();
    let {uid} = await db.fetchOne(`
    SELECT uid
    FROM user
    WHERE uid = ?;
    `, ['5673937042']);
    console.log(uid);
    let result = await db.execute(`
    UPDATE user
    SET scanned = 0
    WHERE uid = ?;
    `, ['5673937042']);
    console.log(result);
})(); */
