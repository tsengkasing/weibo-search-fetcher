# weibo-fetcher

![](https://img.shields.io/badge/puppeteer--core-1.12-brightgreen.svg?style=flat-square) ![](https://img.shields.io/badge/mysql2-1.16-brightgreen.svg?style=flat-square)

## :rocket: 开始

### :package: 依赖安装

如果你没有安装 Node.js, 请先移步 [Node.js/Download](https://nodejs.org/en/download/) 下载安装。

```shell
$ git clone https://gitlab.com/tsengkasing/weibo-search-fetcher.git
$ cd weibo-search-fetcher
$ npm install
```

### :hammer: 配置文件

拷贝一份目录下的 config.template.js 文件并重命名成 config.js，配置 Chrome 的路径。

```shell
$ cp config.template.js config.js
$ vim config.js
```

### :beer: 运行

```shell
$ cd weibo-search-fetcher
$ node index.js
```

PS: if only want to merge the json files in directory 'out', Please type `node index.js merge`.
