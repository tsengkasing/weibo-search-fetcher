/**
 * @fileoverview
 * 根据关键字搜索相关微博
 * 提取用户 id
 */

const fs = require('fs');
const path = require('path');

const WeiboFetcher = require('./weiboFetcher');

(async function main() {
    const [command] = process.argv.slice(2);
    switch (command) {
    case 'open': {
        const fetcher = new WeiboFetcher();
        await fetcher.init({ useDataDir: true });
        await fetcher.open();
    } break;
    case 'location':
        while (true) {
            try {
                const fetcher = new WeiboFetcher();
                await fetcher.init({
                    useDataDir: true,
                    openDevTools: true,
                    // headless: true,
                });
                await fetcher.obtainLocation();
                await fetcher.close();
            } catch (err) { console.error(err); }
        } break;
    case 'follow':
        while (true) {
            try {
                const fetcher = new WeiboFetcher();
                await fetcher.init({
                    useDataDir: true,
                    openDevTools: true,
                    // headless: true,
                });
                await fetcher.followers();
                await fetcher.close();
            } catch (err) { console.error(err); }
        } break;
    case 'search':
        try {
            const fetcher = new WeiboFetcher();
            await fetcher.init();
            await fetcher.search(20);
            await fetcher.close();
        } catch (err) { console.error(err); }
        break;
    case 'merge':
        const fileLocation = mergeFile();
        console.log(`File Save in ${fileLocation}`);
        break;
    }
})();

/**
 * 合并文件结果
 */
function mergeFile() {
    const d = new Date();
    let userIds = fs.readdirSync(path.join(__dirname, 'out'))
        .filter(fileName => !fileName.startsWith('.') && !/weibo-uids/.test(fileName) && !/followers/.test(fileName))
        .map(fileName => fs.readFileSync(path.join(__dirname, 'out', fileName).toString()))
        .map(content => JSON.parse(content))
        .flatMap(users => users.map(({userId}) => userId));

    // 去重
    console.log(`Total ${userIds.length} uids.`);
    userIds = Array.from(new Set(userIds));
    console.log(`Obtained ${userIds.length} uids.`);

    const sqls = userIds.map(id => `INSERT INTO user (uid, scanned, timestamp) VALUES (${id}, '0', ${(d.getTime() / 1000) | 0});`);

    // 存储
    const fileLocation = path.join(__dirname, 'out', `weibo-uids.sql`);
    fs.writeFileSync(
        fileLocation,
        sqls.join('\n'),
        { flag: 'w' }
    );
    return fileLocation;
}
