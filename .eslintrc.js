module.exports = {
    'env': {
        'browser': true,
        'es6': true,
        'node': true,
    },
    'extends': 'standard',
    'rules': {
        'indent': ['error', 4],
        'comma-dangle': 0,
        'semi': [2, 'always'],
        'space-before-function-paren': 0,
        'camelcase': 0,
        'no-inner-declarations': 0,
        'quotes': 0,
        'object-curly-spacing': 0,
        "no-global-assign": 0
    }
};