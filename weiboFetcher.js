/**
 * @fileoverview
 * 使用 Chrome 浏览器
 * 根据关键字
 * 自动爬取微博搜索页结果
 */

const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer-core');

const DB = require('./lib/db');

const { CHROME_PATH, KEYWORD, DB: {HOST, USER, PASSWORD, DATABASE} } = require('./config.json');

class WeiboFetcher {
    constructor() {
        /** @type {puppeteer.Browser} */
        this._browser = null;
    }
    async init({headless = false, openDevTools, useDataDir} = {headless: false}) {
        this._browser = await puppeteer.launch({
            executablePath: CHROME_PATH,
            headless: headless, // 是否显示浏览器
            slowMo: 50, // 延迟每步操作 (毫秒)
            defaultViewport: { width: 1024, height: 1024 },
            devtools: openDevTools,
            userDataDir: useDataDir ? path.resolve(__dirname, 'test-user-data-dir') : null,
        });
    }

    async close() {
        await this._browser.close();
    }

    async open() {
        await this._browser.newPage();
    }

    /**
     * 每次抓搜索结果前 20 页
     * @param {number} times 爬取次数
     */
    async search(times = 10) {
        if (!this._browser) throw new Error('You should init the instance first.');
        const browser = this._browser;

        while (times-- > 0) {
            console.log(`[INFO] Running iteration ${times}`);
            const page = await browser.newPage();
            let weiboList = [];
            try {
                weiboList = await searchUser(page);
            } catch (err) {
                console.error(`[Error] Failed iteration ${times}.`);
                console.error(err);
                continue;
            }
            await page.close();

            // 保存到 out 目录下
            fs.writeFileSync(
                path.join(__dirname, 'out', `weibo-${KEYWORD}-${generateTimeFormat()}.json`),
                JSON.stringify(weiboList),
                { flag: 'w' }
            );
        }
    }

    /**
     * 补充地理位置
     */
    async obtainLocation() {
        if (!this._browser) throw new Error('You should init the instance first.');

        // 连接数据库
        const db = new DB(HOST, USER, PASSWORD, DATABASE);
        await db.connect();
        const rows = await db.fetchAll('SELECT `uid` FROM `user` WHERE `location` IS NULL AND NOT `scanned` = "2" LIMIT 18;');
        log(`Queue: ${rows.length}`);

        const browser = this._browser;
        const page = (await browser.pages())[0];
        const weibosNumSelector = '#Pl_Core_T8CustomTriColumn__3 > div > div > div > table > tbody > tr > td:nth-child(3) > a strong';
        const locationSelector = '#Pl_Core_UserInfo__6 > div > div > div.WB_innerwrap > div > div > ul > li:nth-child(1) > span.item_text.W_fl';

        let errorCount = 0;
        for (const {uid} of rows) {
            try {
                await page.goto(`https://www.weibo.com/u/${uid}`, {waitUntil: 'networkidle2'});
                try {
                    await page.waitForSelector(weibosNumSelector);
                } catch (err) {
                    log(`${uid} May be an Official Account`);
                    await db.execute('UPDATE user SET scanned = 2 WHERE uid = ?;', [uid]);
                    continue;
                }
                let info = await page.evaluate((weibosNumSelector, locationSelector) => {
                    console.log(document.querySelector(locationSelector));
                    const info = [null, null];
                    let $ele = document.querySelector(locationSelector);
                    if (!$ele || /Lv/.test($ele.textContent)) {
                        $ele = document.querySelector('#Pl_Core_UserInfo__6 > div > div > div > div > div > ul > li:nth-child(2) > span.item_text.W_fl');
                    }
                    if ($ele) info[0] = $ele.textContent.trim();
                    $ele = document.querySelector(weibosNumSelector);
                    if ($ele) info[1] = $ele.textContent.trim();
                    console.log(info);
                    return info;
                }, weibosNumSelector, locationSelector);
                if (!info[0]) continue;
                // 完成一个用户
                await db.execute('UPDATE `user` SET `location` = ?, `num_weibo` = ? WHERE `uid` = ?;', [info[0], info[1], uid]);
                log(`Finished ${uid}`);
                errorCount = 0;
            } catch (err) {
                errorCount++;
                console.error(err);
                if (errorCount > 10) {
                    console.log('登录状态失效。');
                    await page.close();
                    await browser.close();
                    process.exit(1);
                }
            }
        }
    }

    /**
     * 递归获取粉丝用户 id
     * @param {string} beginUserId 初始用户 id
     * @param {number} maxNum 最大用户数
     */
    async followers(maxNum = 18) {
        if (!this._browser) throw new Error('You should init the instance first.');

        // 连接数据库
        const db = new DB(HOST, USER, PASSWORD, DATABASE);
        await db.connect();
        let {uid} = await db.fetchOne('SELECT * FROM `user` WHERE `scanned` = "0";');
        log(`Begin User [${uid}]`);

        const browser = this._browser;
        const page = (await browser.pages())[0];
        await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36');

        const followersButton = '#Pl_Core_T8CustomTriColumn__3 > div > div > div > table > tbody > tr > td:nth-child(2) > a';
        const followersList = 'div.follow_box > div.follow_inner > ul';

        let iteration = 0;
        const queue = [uid];
        while (queue.length > 0 && iteration++ < maxNum) {
            const userId = queue.shift(queue);
            try {
                let ok = true;
                await page.goto(`https://www.weibo.com/u/${userId}`, {waitUntil: 'networkidle2'});
                try {
                    await page.waitForSelector(followersButton);
                    await page.click(followersButton);
                } catch (err) {
                    log(`${userId} May be an Official Account`);
                    await db.execute('UPDATE user SET scanned = 2 WHERE uid = ?;', [userId]);
                    ok = false;
                }
                let totalPage = 5;
                while (totalPage-- > 0 && ok) {
                    try {
                        let pages = await browser.pages();
                        if (pages.length > 0) {
                            for (let i = 1; i < pages.length; ++i) {
                                await pages[i].close();
                            }
                        }
                        await page.waitForSelector(followersList);
                    } catch (err) {
                        console.error(err);
                        ok = false;
                        break;
                    }
                    let {ids, next} = await page.evaluate((followersList) => {
                        const $elements = document.querySelectorAll(`${followersList} > li`);
                        let _ids = [];
                        for (let $ele of $elements) {
                            const $link = $ele.querySelector('.clearfix > .mod_pic > a');
                            if (!$link) continue;
                            let result = /\/([0-9]*)\?/.exec($link.href);
                            if (!result) continue;
                            const info = {id: result[1], location: '', numWeibo: -1};
                            const $location = $ele.querySelector('.clearfix > .mod_info.S_line1 > .info_add span');
                            if ($location) {
                                info['location'] = $location.textContent;
                            }
                            const $numWeibo = $ele.querySelector('.clearfix > .mod_info.S_line1 > .info_connect > span:nth-child(3) > em > a');
                            if ($numWeibo) {
                                info['numWeibo'] = $numWeibo.textContent;
                            }
                            _ids.push(info);
                        }
                        let hasNext = false;
                        let $next = document.querySelector('div.WB_cardpage.S_line1 > div > a.page.next.S_txt1.S_line1');
                        if ($next && $next.href) hasNext = true;
                        return {ids: _ids, next: hasNext};
                    }, followersList);
                    if (next) {
                        await page.click('div.WB_cardpage.S_line1 > div > a.page.next.S_txt1.S_line1');
                        await page.waitFor(5000);
                    } else {
                        break;
                    }
                    // 批量插入新的用户
                    const d = new Date();
                    const sql = ids.map(({id, location, numWeibo}) => `("${id}", "0", ${(d.getTime() / 1000) | 0}, "${location}", "${numWeibo}")`).join(', ');
                    await db.execute(`INSERT INTO user (uid, scanned, timestamp, location, num_weibo) VALUES ${sql};`, []);
                    log(`Added ${ids.length} users`);
                }
                if (ok) {
                    // 完成一个用户
                    await db.execute('UPDATE user SET scanned = 1 WHERE uid = ?;', [userId]);
                    log(`Finished ${userId}`);
                }
                ({uid} = await db.fetchOne('SELECT * FROM `user` WHERE `scanned` = "0";'));
                queue.push(uid);
            } catch (err) {
                console.error(`Failed at iteration ${iteration}`);
                console.error(err);
            }
            log(`Iteration ${iteration}`);
        }
        await page.close();
    }
}

module.exports = WeiboFetcher;

/**
 * Search For User
 * @param {puppeteer.Page} page
 */
async function searchUser(page) {
    await page.goto('https://weibo.com', {waitUntil: 'networkidle2'});
    await page.goto(`https://s.weibo.com/weibo?q=${KEYWORD}&wvr=6&b=1&Refer=SWeibo_box`, {waitUntil: 'networkidle2'});

    /** @type {Array<{userProfileLink: string, userNickName: string, weiboText: string, weiboTime: string, userDevice: string}>} */
    let weiboList = [];
    for (let pageNum = 0; pageNum < 20; ++pageNum) {
        let retryTime = 3;
        while (retryTime-- > 0) {
            let ok = true;
            try {
                await page.waitForSelector('#pl_feedlist_index .card-wrap', { timeout: 10000 });
            } catch (err) {
                console.error(err);
                ok = false;
            }
            if (!ok) await page.reload();
            else break;
        }
        if (retryTime < 0) break;

        const weiboListOfPage = await page.evaluate((pageNum) => {
            const weibos = Array
                .from(document.querySelectorAll('div.card-wrap[action-type=feed_list_item]'))
                // 过滤热门和热门文章
                .filter($element => !$element.firstElementChild.classList.contains('card-top'));
            return weibos.map($element => {
                const $card = $element.querySelector('.card').querySelector('.card-feed');
                const $textElement = $card.querySelector('.content > p.txt');

                // 用户 uid
                /** @type {string} */
                let userId = 'NOTFOUND';
                let result = /\/([0-9]*)\?/.exec($card.querySelector('.avator > a').href);
                if (result) {
                    userId = result[1];
                }
                // 用户昵称
                const userNickName = $textElement.getAttribute('nick-name');
                return { userId, userNickName };
            });
        }, pageNum);
        console.log(weiboList.map(id => JSON.stringify(id, null, 2)).join('\n'));
        weiboList = weiboList.concat(weiboListOfPage);
        await page.click('#pl_feedlist_index > div.m-page > div > a.next');
    }
    return weiboList;
}

function generateTimeFormat() {
    const d = new Date();
    return `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}-${d.getHours()}-${d.getMinutes()}-${d.getSeconds()}`;
}

function log(msg) {
    console.log(`[${(new Date()).toLocaleString()}][INFO] ${msg}`);
}
